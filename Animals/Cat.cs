﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Cat:Animal
    {
      
        public Cat()
        {
            this.name = "Cat";
            this.action = " makes sound meow meow!\n";
            
        }

        public override String Action()
        {
          
            return name+action;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Snake : Animal
    {
        public Snake()
        {
            this.name = "Snake";
            this.action = "makes sound hiss,hiss!\n";
        }
        
        public override String Action()
        {
           
            return name+action;
        }
    }
}

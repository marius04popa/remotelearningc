﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animals
{
    class Duck : Animal
    {
        public Duck()
        {
            this.name = "Duck";
            this.action = "makes sound qack qack";
        }
        public override String Action()
        {
            
           // Console.WriteLine( "Duck makes sound qack,qack!\n");
            return name+action;
        }
    }
}

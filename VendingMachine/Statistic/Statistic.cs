﻿using System;

namespace VendingMachine
{
    public class Statistic : IObserver
    {
        private StatisticItem item;
        private ContainableItemCollection containableItem;
        private StatisticCollection statisticCollection;
        private Message message;
        public Statistic(ContainableItemCollection containableItem)
        {
            message = new Message();
            this.containableItem = containableItem;
            item = new StatisticItem();
            statisticCollection = new StatisticCollection();
        }
        public StatisticCollection GetStatisticCollection()
        {
            return statisticCollection;
        }

        public void Update(int id)
        {
            message.YourMessage("Statistic is updated");
            if (containableItem.GetItemCollection(id).product.idProduct == id)
            {
                statisticCollection.UpdateStatistic(containableItem.GetItemCollection(id));
            }
            else
            {
                throw new Exception("Don't update");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace VendingMachine
{
    public class StatisticCollection
    {
        private List<StatisticItem> list;

        public StatisticCollection()
        {
            list = new List<StatisticItem>();
        }

        public void  AddItemStatistic(ContainableItem containable)
        {
            StatisticItem item = new StatisticItem();
            item.product = containable.product;
            item.numberofsoldproduct = 0;
            item.profit = 0;
            list.Add(item);
        }
        public List<StatisticItem> GetList()
        {
            return list;
        }

        public bool Exist(ContainableItem containable)
        {
            if (list.Count() == 0)
            {
                return false;
            }
            else if (list.FirstOrDefault().product.idProduct == containable.product.idProduct)
            {

                    return true;           
            }
            else
            {
                return false;
            }
        }

        public void CounterSold()
        {
            foreach(StatisticItem i in list)
            {
                i.numberofsoldproduct++;
            }
        }

        public void Profit()
        {
            foreach (StatisticItem i in list)
            {
                i.profit = i.product.price / 10;
            }
        }

        public int AllSold()
        {
            int sum = 0;
            foreach (StatisticItem i in list)
            {

                sum=sum+i.numberofsoldproduct;
            }
            return sum;
        }

        public double AllProfit()
        {
            double sum = 0;
            foreach (StatisticItem i in list)
            {
                sum = sum+(i.profit * i.numberofsoldproduct);
            }
            return sum;
        }

        public StatisticItem GetItemStatistic(int id)
        {
            if (list.FirstOrDefault().product.idProduct == id)
            {
                return list.FirstOrDefault();
            }
            else
            {
                return null;
            }
        }

        public void Update()
        {
            CounterSold();
            Profit();
        }

        public void UpdateStatistic(ContainableItem containable)
        {
            if (Exist(containable))
            {
                Update();
            }
            else
            {
                AddItemStatistic(containable);
                Update();
            }
        }
    }
}

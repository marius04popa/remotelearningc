﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
   public interface IObservable
    {
        void Add(IObserver observer);
        void Remove(IObserver observer);
        void notify(int id);  
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public class DispenserComponent :IObservable 
    {

        private ContainableItemCollection containableItemCollection;
        private List<IObserver> list;

        public DispenserComponent(ContainableItemCollection containableItemCollection)
        {
            this.containableItemCollection = containableItemCollection;
            list = new List<IObserver>();
        }

        public void Add(IObserver observer)
        {
            if(!list.Contains(observer))
            {
                list.Add(observer);
            } 
        }

        public void notify(int id)
        {
            list.ForEach(x => x.Update(id));
        }

        public void ProductOrder(int id)
        {
            notify(id);
        }

        public void Remove(IObserver observer)
        {
           if(list.Contains(observer))
            {
                list.Remove(observer);
            }
        }
    }
}

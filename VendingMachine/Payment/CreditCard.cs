﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public class CreditCard
    {
        private Random rnd;
        private int pin;
        private double value;

        public CreditCard()
        {
            rnd = new Random();
            pin = 1234;
            value = rnd.Next(100);
        }
        public bool validation(int pin)
        {
            if (this.pin == pin && value > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void credit(ContainableItemCollection item,int id)
        {
            value = value - item.GetItemCollection(id).product.price;
            
        }   
        public double getCredit()
        {
            return value;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public class PaymentTerminal
    {
        private DispenserComponent dispenserComponent;
        private ContainableItemCollection containableItemCollection;
        private Message message;
        public double amount;

        public PaymentTerminal(DispenserComponent dispenser)
        {
            message = new Message();
            amount = 0;
            this.dispenserComponent =dispenser;

        }
        public double YourCredit()
        {
            return amount;
        }
        public void ResetAmount()
        {
            amount = 0;
        }
        public void AddToAmount(Payment payment)
        {
            amount =amount+ payment.values;
        }
        public double GiveChange(ContainableItem containable)
        {

            return (amount - containable.product.price);

        }
        public Boolean isSold(ContainableItem containable)
        {

            if (containable.product.price == amount)
            {

                return true;
            }
            else
            {

                return false;
            }
        }
        public void transaction(ContainableItem containable, int id)
        {
           
            if (isSold(containable))
            {
                dispenserComponent.ProductOrder(id);
            }
            else if (amount > containable.product.price)
            {
                dispenserComponent.ProductOrder(id);
                message.YourMessage("Your change is:" + GiveChange(containable));
            }
            ResetAmount();

        }

    }
}

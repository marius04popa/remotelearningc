﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
  
    public class ContainableItemCollection :IObserver
    {
        private List<ContainableItem> products_list;
        private Message message;
        private String path = @"F:\Workspace\remotelearningc\VendingMachine\Products.txt";
        private BinaryFormatter bin = new BinaryFormatter();

        public ContainableItemCollection()
        {
            message = new Message();
            products_list=products_list=new List<ContainableItem>();
        }
        public String getPath()
        {
            return path;
        }
        public void AddItemCollection(ContainableItem containableItem)
        {
            
            try
            {
                Stream stream = File.Open(path, FileMode.Open);
               
                using (stream)
                {
                   
                    if (products_list.Any())
                    {
                        if (products_list.FirstOrDefault().position.row == containableItem.position.row
                            && products_list.FirstOrDefault().position.column == containableItem.position.column)
                        {
                          message.YourMessage("Exist a product in the same position");
                        }
                        else if(products_list.FirstOrDefault().product.idProduct == containableItem.product.idProduct)
                        {
                            message.YourMessage("Exist a product with the same id");
                        }
                        else
                        {
                            products_list.Add(containableItem);
                          
                        }
                    }
                    else
                    {
                        products_list.Add(containableItem);
                    
                    }
                    bin.Serialize(stream, products_list);
                    
                }
                stream.Close();

            }
            catch (Exception )
            {
                message.YourMessage("Error acces file");
              
            }
        }
        public void DeleteItemCollectionPosition(Position containableItem)
        {
            try
            {
                Stream stream = File.Open(path, FileMode.Open);
                using (stream)
                {
                    for (int i = 0; i < products_list.Count(); i++)
                    {
                        if (products_list.FirstOrDefault().position.row == containableItem.row
                            && products_list.FirstOrDefault().position.column == containableItem.column
                            )
                        {
                            message.YourMessage("Product :" + products_list.ElementAt(i).product.name+" will be delete");
                            products_list.RemoveAt(i);
                            bin.Serialize(stream, products_list);
                        }
                        else
                        {
                            message.YourMessage("This item doesn't exist");
                        }
                    }
                    
                    stream.Close();
                }

            }
            catch (Exception)
            {

                message.YourMessage("Error acces file");

            }
        }
        

        public ContainableItem GetItemCollection(int index)
        {
            try
            {
                Stream stream = File.Open(path, FileMode.Open);
                using (stream)
                {
                    products_list = (List<ContainableItem>)bin.Deserialize(stream);
                    var item = products_list.FirstOrDefault().product.idProduct;

                        if (item==index)
                        {
                             return products_list.FirstOrDefault();
                             stream.Close();
                        }
                        else
                        {
                            return null;
                            stream.Close();
                        }
                    
                }
                
                
            }
            catch(Exception)
            {
                message.YourMessage("Error acces file");
            }
            return null;
        }
        public ContainableItem GetAllItemsCollection()
        {
            try
            {
                Stream stream = File.Open(path, FileMode.Open);
              
                using (stream)
                {

                    products_list = (List<ContainableItem>)bin.Deserialize(stream);
                    foreach (ContainableItem item in products_list)
                    {
                        message.YourMessage("Row:" + item.position.row + " Column:" + item.position.column + " Id:" 
                            + item.product.idProduct + " Name:" + item.product.name + " Price is:" + item.product.price + "lei "
                            + "Quantity is:" + item.product.quantity);
                    }
                   
                }
                stream.Close();
            }
            catch (Exception)
            {
                message.YourMessage("Error acces file");
            }
            return null;
        }

        public void Update(int id)
        {
            message.YourMessage("ContainableItemCollection is update");
            try
            {
                Stream stream = File.Open(path, FileMode.Open);

                using (stream)
                {

                    foreach (ContainableItem item in products_list)
                    {
                        if (item.product.idProduct == id)
                        {


                            if (item.product.quantity > 1)
                            {

                                Message();
                                item.product.quantity = item.product.quantity - 1;
                                message.YourMessage("The product " + item.product.name + " is ready");
                                bin.Serialize(stream, products_list);

                            }
                            else if (item.product.quantity == 1)
                            {
                                Message();
                                Position position = new Position();
                                position.column = item.position.column;
                                position.row = item.position.row;
                                message.YourMessage("The product " + item.product.name + " is ready");
                                DeleteItemCollectionPosition(position);
                            }
                            else if (item.product.quantity == 0)
                            {
                                message.YourMessage("Quantity is 0");
                            }


                        }
                    }
                }
                stream.Close();
            }
            catch (Exception)
            {
                message.YourMessage("Error acces file");
            }
        }
        public void Message()
        {
            message.YourMessage("Wait for preparing.....");
            System.Threading.Thread.Sleep(5000);
        }
            
    }
}

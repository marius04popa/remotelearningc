﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    [Serializable()]
    public class Product
    {
        public CategoryProduct category { set; get; }
        public String name { get; set; }
        public int idProduct { get; set; }
        public double price { get; set; }
        public int quantity { get; set; }
        public int size { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    [Serializable()]
    public class Position
    {
        public int row { get; set; }
        public int column { get; set; }
    }
}

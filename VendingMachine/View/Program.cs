﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine

{
    public class Program
    {
       
        static void Main(string[] args)
        {
            ContainableItem containableItem = new ContainableItem();
            ContainableItemCollection containableItemCollection = new ContainableItemCollection();
            Statistic statistic = new Statistic(containableItemCollection);
            DispenserComponent dispenser = new DispenserComponent(containableItemCollection);
            dispenser.Add(containableItemCollection);
            dispenser.Add(statistic);
            PaymentTerminal paymentTerminal = new PaymentTerminal(dispenser);
            View view = new View(containableItemCollection);
            do
            {
                Console.WriteLine("***Admin***");
                Console.WriteLine("1.Add item");
                Console.WriteLine("2.Delete item");
                Console.WriteLine("***Customer***");
                Console.WriteLine("3.Display a item");
                Console.WriteLine("4.Buy a product");
                Console.WriteLine("5.Display all items");
                Console.WriteLine("6.Raport CSV");
                Console.WriteLine("7.Exit");
                int option = Convert.ToInt32(Console.ReadLine());

                switch (option)
                {
                    case 1:
                        Position position = new Position();
                        Console.WriteLine("Enter row value");
                        int row = Convert.ToInt32(Console.ReadLine());
                        position.row = row;
                        Console.WriteLine("Enter column value");
                        int column = Convert.ToInt32(Console.ReadLine());
                        position.column = column;

                        Product product = new Product();
                        Console.WriteLine("Id of product");
                        int idProduct = Convert.ToInt32(Console.ReadLine());
                        product.idProduct = idProduct;
                        Console.WriteLine("Category of product");
                        Console.WriteLine("The types of the product Enum are:1.NON_ALCHOOLIC_DRINK 2.ALCHOOLIC_DRINK 3.FOOD");
                        Console.WriteLine("Choose category");
                        String chooseCategory = Convert.ToString(Console.ReadLine()); ;
                        CategoryProduct categoryProduct = (CategoryProduct)Enum.Parse(typeof(CategoryProduct), chooseCategory);
                        product.category = categoryProduct;
                        Console.WriteLine("Name of product");
                        String name = Convert.ToString(Console.ReadLine());
                        product.name = name;
                        Console.WriteLine("Price of product");
                        double price = Convert.ToDouble(Console.ReadLine());
                        product.price = price;

                        Console.WriteLine("Quantity of product");
                        int quantity = Convert.ToInt32(Console.ReadLine());
                        product.quantity = quantity;

                        Console.WriteLine("Enter size value");
                        int size = Convert.ToInt32(Console.ReadLine());
                        product.size = size;
                        containableItem.product = product;
                        containableItem.position = position;
                        containableItemCollection.AddItemCollection(containableItem);
                        Console.WriteLine("Press Enter ....");
                        Console.ReadLine();
                        break;
                    case 2:
                        Position positionDelete = new Position();
                        ContainableItem containableItemDelete = new ContainableItem();
                        Console.WriteLine("Enter row value");
                        int rowDelete = Convert.ToInt32(Console.ReadLine());
                        positionDelete.row = rowDelete;
                        Console.WriteLine("Enter column value");
                        int columnDelete = Convert.ToInt32(Console.ReadLine());
                        positionDelete.column = columnDelete;
                        
                        
                        containableItemCollection.DeleteItemCollectionPosition(positionDelete);
                        break;
                    case 3:
                        Console.WriteLine("Enter id  value for product display");
                        int index = Convert.ToInt32(Console.ReadLine());
                        
                        {
                            Console.WriteLine(" Name:" + containableItemCollection.GetItemCollection(index).product.name
                                              + " Price:" + containableItemCollection.GetItemCollection(index).product.price + " lei"
                                              + " Quantity:" + containableItemCollection.GetItemCollection(index).product.quantity);
                        }
                       
                        break;
                    case 4:
                        view.Views();
                        break;
                    case 5:
                        containableItemCollection.GetAllItemsCollection();
                        break;
                    case 6:
                        view.CSV();
                        break;     
                    case 7:
                        Environment.Exit(0);
                        break;
                    default:
                        Console.WriteLine("Your option is not valid \n");
                        break;
                }
            } while (true);
    }
    }
}

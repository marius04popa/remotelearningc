﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VendingMachine
{
    public  class View
    {
        ContainableItemCollection containableItemCollection;
        DispenserComponent dispenser;
        PaymentTerminal paymentTerminal;
        Statistic statistic;
       
        public View(ContainableItemCollection containableItemCollection)
        {
            this.containableItemCollection = containableItemCollection;
            dispenser = new DispenserComponent(containableItemCollection);
            dispenser.Add(containableItemCollection);
            paymentTerminal = new PaymentTerminal(dispenser);
            statistic = new Statistic(containableItemCollection);
            dispenser.Add(statistic);
        }


        public void Views()
        {
            Console.WriteLine("Enter id value for the desired product:");
            int id = Convert.ToInt32(Console.ReadLine());
            

            Console.WriteLine("1.Cash");
            Console.WriteLine("2.Card");
            Console.Write("Enter your option to pay:");
            int choise = Convert.ToInt32(Console.ReadLine());
            switch (choise)
            {
                case 1:
                    while (containableItemCollection.GetItemCollection(id).product.price > paymentTerminal.YourCredit())
            {
                        Console.WriteLine("Product price is:" + containableItemCollection.GetItemCollection(id).product.price + " lei");
                        Console.WriteLine("Your credit is:" + paymentTerminal.YourCredit() + " lei");
                        Console.WriteLine("1->0.01 lei");
                        Console.WriteLine("2->0.1 lei");
                        Console.WriteLine("3->0.5 lei");
                        Console.WriteLine("4->1 leu");
                        Console.WriteLine("5->5 lei");
                        Console.WriteLine("6->10 lei");
                        Console.WriteLine("7->Exit");
                        Console.Write("Enter your cash");
                        int option = Convert.ToInt32(Console.ReadLine());
                        switch (option)
                        {
                            case 1:
                                Payment coins001 = new Coin(0.01);
                                paymentTerminal.AddToAmount(coins001);
                                break;
                            case 2:
                                Payment coins01 = new Coin(0.1);
                                paymentTerminal.AddToAmount(coins01);
                                break;
                            case 3:
                                Payment coins05 = new Coin(0.5);
                                paymentTerminal.AddToAmount(coins05);
                                break;
                            case 4:
                                Payment banknote1 = new Banknote(1);
                                paymentTerminal.AddToAmount(banknote1);
                                break;
                            case 5:
                                Payment banknote5 = new Banknote(5);
                                paymentTerminal.AddToAmount(banknote5);
                                break;
                            case 6:
                                Payment banknote10 = new Banknote(10);
                                paymentTerminal.AddToAmount(banknote10);
                                break;
                            case 7:
                                Console.WriteLine("Return your money:" + paymentTerminal.YourCredit() + " lei");
                                return;
                        }

                }

                paymentTerminal.transaction(containableItemCollection.GetItemCollection(id), id);
                break;
                    case 2:
                    Console.WriteLine("Product price is:" + containableItemCollection.GetItemCollection(id).product.price + " lei");
                    CreditCard creditCard = new CreditCard();
                    Console.WriteLine("Enter your pin");
                    int pin= Convert.ToInt32(Console.ReadLine());
                    if(creditCard.validation(pin))
                    {
                        dispenser.ProductOrder(id);
                        creditCard.credit(containableItemCollection, id);
                        Console.WriteLine("Receipt");
                        Console.WriteLine("Initial credit:" + (creditCard.getCredit() + containableItemCollection.GetItemCollection(id).product.price)+"lei");
                        Console.WriteLine("Your Credit is " + creditCard.getCredit()+"lei");
                    }
                    else
                    {
                        Console.WriteLine("Your credit is insufficient");
                    }
                    break;

                default:
                    Console.WriteLine("Your option is invalid");
                    break;
            
        }
            
        }
        public void CSV()
        {
            var list = statistic.GetStatisticCollection();
            DateTime dateTime = DateTime.Now;
            try
            {
                using (System.IO.StreamWriter file =
            new System.IO.StreamWriter(@"RaportCSV", true))
                {

                    foreach (StatisticItem i in list.GetList())
                    {
                        file.WriteLine("");
                        file.WriteLine(dateTime.ToString());
                        file.WriteLine("Name:" + i.product.name +
                            " - Number of sold products:" + i.numberofsoldproduct +
                            " - Profit:" + i.profit + "lei");
                    }
                    file.WriteLine("Number of transaction:" + list.AllSold());
                    file.WriteLine("Overall profit:" + list.AllProfit());


                }
            }catch
            {
                throw new Exception("Error acces file");
            }
            }

        
    }

    
}
